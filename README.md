# Ruby Regexp

Learn Ruby Regexp step by step from beginner to advanced levels with 200+ examples

<p align="center">
    <a href="https://leanpub.com/rubyregexp"><img src="./images/Ruby_regexp.png" width="600px" height="400px" /></a>
</p>

The book also includes exercises to test your understanding, which is presented together
as a single file in this repo - [Exercises.md](./exercises/Exercises.md)

See [Version_changes.md](./Version_changes.md) to keep track of changes made to the book.

<br>

# E-book


* You can download the book from any of these links for free or pay what you wish
    * https://leanpub.com/rubyregexp
    * https://gumroad.com/l/rubyregexp
    * For those in India who wish to pay, visit https://www.instamojo.com/learnbyexample/ruby-regexp/
* You can also get the book as **(Python|Ruby) regex** bundle from https://leanpub.com/b/pythonrubyregex

<br>

# Table of Contents

1. Preface
2. Why is it needed?
3. Regexp literal and operators
4. Anchors
5. Alternation and Grouping
6. Escaping metacharacters
7. Dot metacharacter and Quantifiers
8. Working with matched portions
9. Character class
10. Groupings and backreferences
11. Lookarounds
12. Modifiers
13. String Encoding
14. Miscellaneous
15. Gotchas
16. Further Reading

<br>

# Contributing

* Open an issue for suggestions, bugs, typos, etc

<br>

# License

The code snippets are licensed under MIT, see [LICENSE](./LICENSE) file

