<br>

### 1.1

* changed cover image
* added illustration for recursive matching section
* external link updates and minor description changes

<br>

### 1.0

* added exercises
* some comments for code snippets improved, typos fixed
* AND conditional examples made as a sub-heading
* font for code snippets changed to accommodate some Unicode characters

<br>

### 0.6

* second example for Character class chapter changed from `grep` to `gsub`
* second example for String Encoding chapter changed from `gsub` to `scan`
* better naming for `hash` examples
* Recursive matching simplified to use `\g<0>` instead of capture group and `\g<1>`
    * also, description improved and added links for viewing the regexp online as railroad diagrams

<br>

### 0.5

* First version

